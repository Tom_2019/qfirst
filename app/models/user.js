// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({
  username : String,
  password : String,
  name : String,
  email : String,
  address : String,
  language : String,
  phone : Number,
  companyname:String,
  notes : String,
  status:{type:String,lowercase:true,enum:["active","suspended"]},
  profilePhoto : String,
  permission : {type:String,enum:["SuperAdmin","Admin","Consultant","other"]},
  permit : {type:String,lowercase:true,enum:["verified","notverified"]},
  subpermission : {type:String,enum:["Author","Reveiwer","Approver","User"]},
  reset_key : String,
  verify_key:String,
  jobdescription : String,

});

// generating a password hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);
