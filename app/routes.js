const moment = require('moment');
const fs = require('fs');
var bodyParser = require('body-parser');
const uniqueString = require('unique-string');
const nodemailer = require("nodemailer");
var User = require('./models/user');
const {
  TRANSPORTER_OPTIONS,
  SENDER
} = require("../config/mailer");
module.exports = function(app, passport) {


  // app.get()
  // app.use('/admin', isLoggedIn, isAdmin, require('./routes/admin'))
  // app.use('/manager', isLoggedIn, isManager, require('./routes/manager'))
  // app.use('/promanager', isLoggedIn, ispromanager, require('./routes/promanager'))
  // app.use('/operatmanager', isLoggedIn, isoperatManager, require('./routes/operatmanager'))

  // app.use('/manager', isLoggedIn, isManager, require('./routes/manager'))
  // app.use('/vendor', isLoggedIn, isAccounts, require('./routes/vendor'))


  // dashboard redirect routing
  // app.get('/dashboard', isLoggedIn, function(req, res) {
  //   if (req.user.permission == "admin" )
  //     res.redirect('/admin/dashboard');
  //   else if (req.user.permission == "manager")
  //     res.redirect('/manager/dashboard');
  //   else if (req.user.permission == "promanager")
  //     res.redirect('/promanager/dashboard');
  //     else if (req.user.permission == "operatmanager")
  //       res.redirect('/operatmanager/dashboard')
  //   else {
  //     res.send("<center><h3>You don't have the user permissions to access the software yet.</h3><br <a href='/logout'>Click Here</a> to go back.</center> ")
  //   }
  // })



  // app.get('/verifyemail', function(req, res) {
  //   res.render('customernotpermitted.ejs',{
  //     message: req.flash('signupMessage'),
  //     user : req.user
  //   });
  // });

  // app.get('/dashboard', function(req, res) {
  //   res.render('dashboard.ejs',{
  //     user : req.user
  //   });
  // });

  // LOGOUT ==============================
  // app.get('/logout', function(req, res) {
  //   req.logout();
  //   res.redirect('/');
  // });

  // =============================================================================
  // AUTHENTICATE (FIRST LOGIN) ==================================================
  // =============================================================================

  // locally --------------------------------
  // LOGIN ===============================
  // show the login form
  app.get('/', function(req, res) {
    res.render('loginin.ejs',{
    user : req.user
  });
  });

  app.get('/dashboard', function(req, res) {
   res.render('dashboard.ejs',{
   user : req.user
 });
 });

  app.get('/login', (req, res)=> {
    res.render('login.ejs');
   });
   app.get('/signup', (req, res)=> {
    res.render('signup.ejs');
   });

  app.get('/create/issue', (req, res)=> {
    res.render('create-issue.ejs', {
       moment : moment
      });
   });
   app.get('/add/issue/sources', (req, res)=> {
    res.render('add-issue-sources.ejs', {
       moment : moment
      });
   });
   app.get('/add/issue/standard', (req, res)=> {
    res.render('add-issue-standard.ejs', {
       moment : moment
      });
   });
   app.get('/add/issue/type', (req, res)=> {
    res.render('add-issue-type.ejs', {
       moment : moment
      });
   });
   app.get('/add/issue/titles', (req, res)=> {
    res.render('add-issue-titles.ejs', {
       moment : moment
      });
   });

   app.get('/add/issue/suppliers', (req, res)=> {
      res.render('add-issue-suppliers.ejs', {
         moment : moment
        });
     });
   app.get('/add/issue/tags', (req, res)=> {
    res.render('add-issue-tags.ejs', {
       moment : moment
      });
   });

   app.get('/view/issue', (req, res)=> {
      res.render('view-issues.ejs', {
         moment : moment
        });
     });
   app.get('/create/procedure', (req, res)=> {
    res.render('create-procedure.ejs', {
       moment : moment
      });
   });
   app.get('/add/procedure/category', (req, res)=> {
    res.render('add-procedure-category.ejs', {
       moment : moment
      });
   });
   app.get('/add/procedure/tags', (req, res)=> {
    res.render('add-procedure-tags.ejs', {
       moment : moment
      });
   });
   app.get('/add/procedure/type', (req, res)=> {
    res.render('add-procedure-type.ejs', {
       moment : moment
      });
   });

   app.get('/add/procedure/norms', (req, res)=> {
      res.render('add-norms.ejs', {
         moment : moment
        });
     });

     app.get('/create/new/tools', (req, res)=> {
      res.render('add-new-tools.ejs', {
         moment : moment
        });
     });

     app.get('/view/procedure', (req, res)=> {
      res.render('view-procedures.ejs', {
         moment : moment
        });
     });

     app.get('/add/tool/categories', (req, res)=> {
      res.render('add-tool-categories.ejs', {
         moment : moment
        });
     });

     app.get('/add/tool/tags', (req, res)=> {
      res.render('add-tools-tags.ejs', {
         moment : moment
        });
     });

     app.get('/add/file/tags', (req, res)=> {
      res.render('add-file-tags.ejs', {
         moment : moment
        });
     });

     app.get('/upload/file', (req, res)=> {
      res.render('upload-file.ejs', {
         moment : moment
        });
     });

     app.get('/view/file', (req, res)=> {
      res.render('view-file.ejs', {
         moment : moment
        });
     });

     app.get('/add/form/tags', (req, res)=> {
      res.render('add-form-tags.ejs', {
         moment : moment
        });
     });

     app.get('/add/survey/tags', (req, res)=> {
      res.render('add-survey-tags.ejs', {
         moment : moment
        });
     });


     app.get('/add/tool/inspectiontype', (req, res)=> {
      res.render('add-insepection-type.ejs', {
         moment : moment
        });
     });

     app.get('/add/tool/maintenance', (req, res)=> {
      res.render('add-maintanence-type.ejs', {
         moment : moment
        });
     });

     app.get('/create/tool', (req, res)=> {
      res.render('create-new-tools.ejs', {
         moment : moment
        });
     });

     app.get('/create/task', (req, res)=> {
      res.render('create-task.ejs', {
         moment : moment
        });
     });

     


     app.get('/view/tool', (req, res)=> {
      res.render('view-tools.ejs', {
         moment : moment
        });
     });

     app.get('/add/task/source', (req, res)=> {
      res.render('add-task-source.ejs', {
         moment : moment
        });
     });

     app.get('/add/task/type', (req, res)=> {
      res.render('add-task-type.ejs', {
         moment : moment
        });
     });

     app.get('/add/planning/categories', (req, res)=> {
      res.render('add-meeting-type.ejs', {
         moment : moment
        });
     });

     app.get('/create/planning', (req, res)=> {
      res.render('create-planning.ejs', {
         moment : moment
        });
     });

     app.get('/user/information', (req, res)=> {
      res.render('user-information.ejs', {
         moment : moment
        });
     });

     app.get('/user/profile', (req, res)=> {
      res.render('user-profile.ejs', {
         moment : moment
        });
     });

     app.get('/confirm/user', (req, res)=> {
      res.render('confirm-user.ejs', {
         moment : moment
        });
     });

     app.get('/change/permission', (req, res)=> {
      res.render('change-permission.ejs', {
         moment : moment
        });
     });

     app.get('/view/user', (req, res)=> {
      res.render('view-user.ejs', {
         moment : moment
        });
     });

     app.get('/view/tinymce', (req, res)=> {
      res.render('tinymce.ejs', {
         moment : moment
        });
     });

     

     app.get('/add/employee', (req, res)=> {
      res.render('add-employee.ejs', {
         moment : moment
        });
     });

     app.get('/view/employee', (req, res)=> {
      res.render('view-employee.ejs', {
         moment : moment
        });
     });

     app.get('/add/job', (req, res)=> {
      res.render('add-jobdescription.ejs', {
         moment : moment
        });
     });

     app.get('/add/basic', (req, res)=> {
      res.render('basic-setup.ejs', {
         moment : moment
        });
     });

     app.get('/view/oneissue', (req, res)=> {
      res.render('one-issue.ejs', {
         moment : moment
        });
     });

     app.get('/create/rootcause', (req, res)=> {
      res.render('create-root-cause.ejs', {
         moment : moment
        });
     });

     app.get('/create/test', (req, res)=> {
      res.render('testingpage.ejs', {
         moment : moment
        });
     });

     app.get('/create/datatable', (req, res)=> {
      res.render('datatable.ejs', {
         moment : moment
        });
     });

  // process the login form
}
