// const passport = require('./config/passport');
const app = require('express').Router();
const moment = require('moment');
const uniqueString = require('unique-string');
const nodemailer = require("nodemailer");
const User = require('../models/user');
const Vendor = require('../models/vendor');
const Packvendor = require('../models/packvendor');
const Product = require('../models/product');
const Inventory = require('../models/inventory');
const Blend = require('../models/blend');
const Requestbom = require('../models/requestbom');
const Material = require('../models/material');
const Order = require('../models/order');

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
const {
  TRANSPORTER_OPTIONS,
  SENDER
} = require("../../config/mailer");


// Rout For Dashboard
app.get('/dashboard', (req, res)=> {
  res.render('admin/dashboard.ejs', {
     moment : moment
   });
 });

app.get('/add/users', (req, res)=> {
 res.render('admin/add-user.ejs', {
    user : req.user,
    moment : moment
   });
});

app.get('/change/permission/:id', (req, res)=> {
  User.find({_id:req.params.id},function(err, foundUser) {
  res.render('admin/changepermission.ejs', {
    profile : foundUser,
     user : req.user,
     moment : moment
    });
  });
 });


 app.post('/user/:id', function(req,res){
  User.findOneAndUpdate({_id:req.params.id},{
    $set : {
             permission : req.body.permission,

           }},
           {new: true, upsert:true},
           function(err, changedpermission){
             if(err) {
                        console.log(err);
                     }
             else {
               // console.log(changedpermission);
                   res.redirect('/admin/active/users');
                  }
           }
      )
});


      app.post('/add/user', (req, res)=> {
        User.findOne({username: req.body.username}, function(err, foundUser) {
                      if (foundUser || foundUser != null) {
                        req.flash('user_exist_msg', 'User already exist')
                        // res.redirect('/password/forgot');
                      }else{
                        var key = uniqueString();
                    var new_user = User();
                       new_user.username=req.body.username;
                       new_user.email=req.body.email;
                       new_user.phone=req.body.phone;
                       new_user.permission=req.body.permission;
                       new_user.post=req.body.user;
                       new_user.reset_key=key;
                       new_user.status="active";
                       new_user.save(function(err, user_data){
                              if(err) res.json(err);
                              else{
                                     console.log(user_data);
                                     process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
                                     var transporter = nodemailer.createTransport(TRANSPORTER_OPTIONS);
                                     var mailOptions = {
                                       from: SENDER, // sender address
                                       to: user_data.email, // list of receivers
                                       subject: 'Set Your Password', // Subject line
                                       html: '<div>Please use the link to set your password</div><a href="http://' + req.headers.host + '/user/password/set?key=' + key + '">Click Here</a> to set your Password' // You can choose to send an HTML body instead
                                     };
                                     transporter.sendMail(mailOptions, function(error, info) {
                                       if (error) {
                                         console.log('error: ', error);

                                       } else {
                                         req.flash('setpassword_email_sent', 'Mail sent successfully')
                                         res.redirect('/admin/add/users');
                                       }
                                     });
                                  }
                             });
                           }
                           });
                           });
          //TO display all active users
app.get('/active/users', (req, res)=> {
User.find({status:"active"}, function(err, foundUser) {
  res.render('admin/active-users.ejs', {
     data:foundUser,
     user : req.user,
     moment : moment
    });
   });
  });
  //To suspend the user
  app.get('/suspend/user/:id', (req, res)=> {
  User.updateOne({_id:req.params.id},{$set:{status:"suspended"}}, function(err, foundUser) {
    res.redirect("/admin/active/users");
     });
    });
  //view suspended users
  app.get('/suspended/users', async (req, res)=> {
  let data =await User.find({status:"suspended"}, function(err, foundUser) {
    return foundUser;
  });
    res.render('admin/suspended-users.ejs', {
       data:data,
       user : req.user,
       moment : moment
      });
    });
    //to activate user
    app.get('/activate/user/:id', (req, res)=> {
    User.updateOne({_id:req.params.id},{$set:{status:"active"}}, function(err, foundUser) {
      res.redirect("/admin/suspended/users");
       });
      });
  //to get user password change page
  app.get('/change/user/password/:id', (req, res)=> {
   User.findOne({_id:req.params.id}, function(err, data) {
    res.render('admin/edit-user-password.ejs', {
        data:data,
        user : req.user,
        moment : moment
        });
      });
    });
    //Action to Change user password
    app.post('/change/user/password/:id', (req, res)=> {
     User.updateOne({_id:req.params.id},{$set:{password:User().generateHash(req.body.password)}}, function(err, data) {
     res.redirect("/admin/active/users");
        });
      });

app.get('/add/vendor', (req, res) => {
Product.find({}, function(err, foundProduct){
  res.render('admin/addvendor.ejs', {
      user : req.user,
      product : foundProduct,
      moment : moment
    });
  })
});

app.get('/view/vendor', (req, res)=> {
Vendor.find({}, function(err, foundVendor) {
  res.render('admin/viewvendor.ejs', {
     data:foundVendor,
     user : req.user,
     moment : moment
    });
   });
  });

  app.get('/view/packvendor', (req, res)=> {
    Packvendor.find({}, function(err, foundVendor) {
      res.render('admin/viewvendor_packaging.ejs', {
         data:foundVendor,
         user : req.user,
         moment : moment
        });
       });
      });



  app.get('/add/packvendor', (req, res) => {
    Material.find({}, function(err, foundProduct){
      res.render('admin/addvendor_packaging.ejs', {
          user : req.user,
          product : foundProduct,
          moment : moment
        });
      })
    });

    app.get('/view/packvendor', (req, res)=> {
    Vendor.find({}, function(err, foundVendor) {
      res.render('admin/viewvendor_packaging.ejs', {
         data:foundVendor,
         user : req.user,
         moment : moment
        });
       });
      });

  //AJAX TO GET PRUDUNT NAME OF SELECTED Category
  app.get('/category/products/:cat', (req, res)=> {
  Product.findOne({product_category:req.params.cat}, function(err, data) {
    res.send({
       data:data
      });
     });
    });

    app.get('/category/packproducts/:cat', (req, res)=> {
      Material.findOne({product_category:req.params.cat}, function(err, data) {
        res.send({
           data:data
          });
         });
        });

    //AJAX TO GET ALL Category
    app.get('/all/categories', (req, res)=> {
    Product.find({}, function(err, data) {
      res.send({
         data:data
        });
       });
      });

      app.get('/all/packcategories', (req, res)=> {
        Material.find({}, function(err, data) {
          res.send({
             data:data
            });
           });
          });

  app.get('/view/product', (req, res)=> {
  Product.find({}, function(err, foundProduct) {
    res.render('admin/viewproduct.ejs', {
       data:foundProduct,
       moment : moment
      });
     });
    });
    app.get('/edit/selected/product', (req, res)=> {
    Product.findOne({_id:req.query.a, "products._id":req.query.b},{ "products.$" : 1}, function(err, foundProduct) {
      Product.findOne({_id:req.query.a}, function(err, categorydata) {
      console.log(foundProduct);
      res.render('admin/edit_selected_product.ejs', {
         data:foundProduct,
         moment : moment,
         category:categorydata
         });
        });
       });
      });
app.post('/edit/product', (req, res)=> {
Product.updateOne({_id:req.query.a, "products._id":req.query.b},{$set:{  "products.$.productname" : req.body.productname,"products.$.productdescr" :req.body.productdescr}}, function(err, foundProduct) {
  console.log(foundProduct);
  res.redirect('/admin/view/product');
   });
  });
  app.get('/delete/product', (req, res)=> {
  Product.updateOne({_id:req.query.a},{$pull:{products:{_id:req.query.b}}}, function(err, foundProduct) {
    if(err) throw err;
    console.log(foundProduct);
    res.redirect('/admin/view/product');
     });
    });


  app.post('/add/vendor', function(req, res){
    console.log(req.body);

    var vendor = new Vendor();
      vendor.name = req.body.name;
      vendor.email = req.body.email;
      vendor.phone = req.body.phone;
      // vendor.comment = req.body.comment;
      vendor.state = req.body.state;
    let arr=[];
    let product_category=arr.concat(req.body.product_category);
    let productname=arr.concat(req.body.productname);
    let productprice=arr.concat(req.body.productprice);
    let productremarks=arr.concat(req.body.productremarks);
    let productstatus=arr.concat(req.body.productstatus);
    let addedby=arr.concat(req.body.addedby);
    vendor.save(function(err, doc){
      if(err) res.json(err);
      else {
        console.log(doc);
        for(var i=0;i<productname.length;i++){
        Vendor.updateOne({_id:doc.id},{$push:{product_info:{product_category:product_category[i],productname:productname[i],productprice:productprice[i],productremarks:productremarks[i],productstatus:productstatus[i],addedby:addedby[i]}}},function(err,data){
        if(err) res.json(err);
        else{
          console.log(data);
        }
        })
      }
      res.redirect('/admin/view/vendor'); // redirect to the admin page
      }
    });
  });


  app.post('/add/packagingvendor', function(req, res){
    console.log(req.body);

    var vendor = new Packvendor();
      vendor.name = req.body.name;
      vendor.email = req.body.email;
      vendor.phone = req.body.phone;
      // vendor.comment = req.body.comment;
      vendor.state = req.body.state;
    let arr=[];
    let product_category=arr.concat(req.body.product_category);
    let raw_material_name=arr.concat(req.body.raw_material_name);
    let cost=arr.concat(req.body.cost);
    let remarks=arr.concat(req.body.remarks);
    let status=arr.concat(req.body.status);
    let addedby=arr.concat(req.body.addedby);
    vendor.save(function(err, doc){
      if(err) res.json(err);
      else {
        console.log(doc);
        for(var i=0;i<raw_material_name.length;i++){
        Packvendor.updateOne({_id:doc.id},{$push:{product_info:{product_category:product_category[i],raw_material_name:raw_material_name[i],cost:cost[i],remarks:remarks[i],status:status[i],addedby:addedby[i]}}},function(err,data){
        if(err) res.json(err);
        else{
          console.log(data);
        }
        })
      }
      res.redirect('/admin/view/packvendor'); // redirect to the admin page
      }
    });
  });

   app.get('/edit/selected/vendor/:id', (req, res) => {
   Product.find({}, function(err, foundProduct){
     Vendor.findOne({_id:req.params.id}, function(err, data){
     res.render('admin/edit_vendor.ejs', {
         user : req.user,
         product : foundProduct,
         moment : moment,
         data:data
         });
       });
     })
   });

   app.post('/edit/vendor/:id', function(req, res){
     console.log(req.body);

     var vendor = new Vendor();
       vendor.name = req.body.name;
       vendor.email = req.body.email;
       vendor.phone = req.body.phone;
       vendor.comment = req.body.comment;
     let arr=[];
     let product_category=arr.concat(req.body.product_category);
     let productname=arr.concat(req.body.productname);
     let productprice=arr.concat(req.body.productprice);
         for(var i=0;i<productname.length;i++){
           arr.push({product_category:product_category[i],productname:productname[i],productprice:productprice[i]})
           }
         Vendor.updateOne({_id:req.params.id},{$set:{name:req.body.name,email:req.body.email,phone:req.body.phone,comment:req.body.comment,product_info:arr}},function(err,data){
         if(err) res.json(err);
           console.log(data);
           res.redirect('/admin/view/vendor'); // redirect to the admin page
         })

   });

   app.get('/delete/vendor/:id',(req,res)=>{
     let vendor = Vendor.deleteOne({_id:req.params.id});
 vendor.exec((err,resultssss) =>{
 if(err) throw err;
 res.redirect('/admin/view/vendor');
})
   })
  app.get('/view/all/categories', (req, res) => {
    Product.find({},function(err, data){
    res.render('admin/view_all_categories.ejs', {
      user : req.user,
      moment : moment,
      data:data
    })
  })
  });
  app.get('/edit/selected/category/:id', (req, res)=> {
   Product.findOne({_id:req.params.id}, function(err, data) {
     console.log(data);
    res.render('admin/edit_category.ejs', {
        data:data,
        user : req.user,
        moment : moment
        });
      });
    });
    app.post('/edit/category/:id', (req, res)=> {
     Product.updateOne({_id:req.params.id},{$set:{product_category:req.body.product_cat}}, function(err, data) {
      res.redirect('/admin/view/all/categories');
        });
      });

      app.get('/delete/category/:id',(req,res)=>{
        let cat = Product.deleteOne({_id:req.params.id});
    cat.exec((err,resultssss) =>{
    if(err) throw err;
    res.redirect('/admin/view/all/categories');
  })
      })
  app.get('/add/product', (req, res) => {
    Product.find({},function(err, data){
    res.render('admin/addproduct.ejs', {
      user : req.user,
      moment : moment,
      data:data
    })
  })
  });

  app.post('/add/product', function(req, res){
    console.log(req.body);
    Product.updateOne({product_category:req.body.product_category},{$push:{products:{productname:req.body.productname,productdescr:req.body.productdescr}}},function(err, docsss){
      if(err) res.json(err);
      else {
        console.log(docsss);
        res.redirect('/admin/add/product'); // redirect to the admin page
      }
    });
  });

  app.get('/add/products/inventory', (req, res) => {
    console.log(req.query.a);
    console.log(req.query.b);
    Vendor.findOne({_id:req.query.a,"product_info._id":req.query.b},{ name:1 , "product_info.$" : 1}, function(err, foundVendor){
      Vendor.updateOne({_id:req.query.a,"product_info._id":req.query.b},{$set:{"product_info.$.orderstatus":"open"}}, function(err, foundVendorse){
     console.log("mrinal",foundVendor);
     console.log("tushar",foundVendorse);
      res.render('admin/order_product.ejs', {
          vendor : foundVendor,
          moment : moment
      });
      });
    })
    });

  app.get('/add/product/category', (req, res) => {
    res.render('admin/add-product-category.ejs', {
      user : req.user,
      moment : moment
    })
  });

  app.get('/add/inventorytype', (req, res) => {
    res.render('admin/addinventorytype.ejs', {
      user : req.user,
      moment : moment,
    })

  });


app.post('/add/inventorytype', function(req, res){
    console.log(req.body);
    Product.updateOne({$push:{inventory_type:{productname:req.body.productname,productdescr:req.body.productdescr}}},function(err, docsss){
      if(err) res.json(err);
      else {
        console.log(docsss);
        res.redirect('/admin/add/product'); // redirect to the admin page
      }
    });
  });

app.get('/add/procurmentproduct/category', (req, res) => {
    res.render('admin/add-product-category.ejs', {
      user : req.user,
      moment : moment
    })
  });

app.post('/add/procurmentproduct/category', function(req, res){
    console.log(req.body);
    var product = new Product();
    product.product_category = req.body.product_cat;
    product.save(function(err, docsss){
      if(err) res.json(err);
      else {
        res.redirect('/admin/add/product/category'); // redirect to the admin page
      }
    });
  });

app.get('/add/procurmentproduct', (req, res) => {
    Product.find({},function(err, data){
    res.render('admin/addproduct.ejs', {
      user : req.user,
      moment : moment,
      data:data
    })
  })
  });

app.post('/add/procurmentproduct', function(req, res){
    console.log(req.body);
    Product.updateOne({product_category:req.body.product_category},{$push:{products:{productname:req.body.productname,productdescr:req.body.productdescr,producttype:"procurment"}}},function(err, docsss){
      if(err) res.json(err);
      else {
        console.log(docsss);
        res.redirect('/admin/add/procurmentproduct'); // redirect to the admin page
      }
    });
  });

app.get('/add/packagingproduct/category', (req, res) => {
    res.render('admin/add-packaging-category.ejs', {
      user : req.user,
      moment : moment
    })
  });

app.post('/add/packagingproduct/category', function(req, res){
    console.log(req.body);
    var material = new Material();
    material.product_category = req.body.product_cat;
    material.save(function(err, docsss){
      if(err) res.json(err);
      else {
        res.redirect('/admin/add/packagingproduct/category'); // redirect to the admin page
      }
    });
  });

//To Add Raw material
app.get('/add/packagingproduct', (req, res)=> {
  Material.find({},function(err, data){
 res.render('admin/add-raw-material.ejs', {
      user : req.user,
      moment : moment,
      data :data
    });
  })
  });

//to Add Raw material /add/row/material
app.post('/add/packagingproduct', function(req, res){
  console.log(req.body);
  Material.updateOne({product_category:req.body.product_category},{$push:{products:{raw_material_name:req.body.raw_material_name}}},function(err, docsss){
    if(err) res.json(err);
    else {
      console.log(docsss);
      res.redirect('/admin/add/packagingproduct'); // redirect to the admin page
    }
  });
});


app.get('/view/procurmentproduct', (req, res)=> {
  Product.find({}, function(err, foundProduct) {
   res.render('admin/viewproduct.ejs', {
     data:foundProduct,
     moment : moment
    });
   });
 });

 app.get('/view/packagingproduct', (req, res)=> {
  Material.find({}, function(err, foundMaterial) {
   res.render('admin/viewpackagingproduct.ejs', {
     data:foundMaterial,
     moment : moment
    });
   });
 });


                 //TO display raw material data
// app.get('/raw/materials', (req, res)=> {
//  Material.find({}, function(err, data) {
//   res.render('admin/added-raw-material.ejs', {
//       data:data,
//       user : req.user,
//       moment : moment
//       });
//     });
//   });


 //Display add product to inventory page
app.get('/add/product/inventory', (req, res) => {
 Product.find({}, function(err, foundProduct){
   res.render('admin/product-in-inventory.ejs', {
       product : foundProduct,
       moment : moment
     });
   })
 });


 //To get all vendors selling that product
app.get('/products/vendors/:pname', (req, res)=> {
   Vendor.find({"product_info.productname":req.params.pname}, function(err, data) {
   console.log(data);
   res.send({
      data:data
     });
    });
   });


app.get('/vendors/product/price', (req, res)=> {
     Vendor.findOne({name:req.query.v,"product_info.productname":req.query.p}, function(err, data) {
     console.log(data);
     let kj;
     data.product_info.forEach((info)=>{
       if(info.productname==req.query.p){
         kj=info.productprice;
       }
     })
     res.send({
        data:kj
       });
      });
     });

     //Add products to inventory
app.post('/add/product/inventory', (req, res)=> {
       Inventory.findOne({product_category:req.body.product_category,productname:req.body.productname}, function(err, founddata) {
         if(founddata){
          Inventory.updateOne({product_category:req.body.product_category,productname:req.body.productname},{$set:{productqty:+req.body.productqty + +founddata.productqty,productvendor:req.body.productvendor,productprice:req.body.productprice,update_date:new Date()}}, function(err, done) {
           console.log(done);
           res.redirect("/admin/add/product/inventory");
          })
         }else{
        var invent = Inventory();
         invent.product_category=req.body.product_category;
         invent.productname=req.body.productname;
         invent.productqty=req.body.productqty;
         invent.productvendor=req.body.productvendor;
         invent.productprice=req.body.productprice;
         invent.update_date=new Date();
         invent.save(function(err, data){
          if(err) res.json(err);
             else{
             console.log(data);
             res.redirect("/admin/add/product/inventory");
                   }
              });
            }
            })
         });
         //Dispaly Inventories data
app.get('/view/inventory', (req, res)=> {
           Inventory.find({}, function(err, data) {
           console.log(data);
           res.render("admin/view-inventory.ejs",{
              data:data,
              moment : moment
             });
          })
          });
          //Display add product to inventory page
app.get('/add/blend', (req, res) => {
          Product.find({}, function(err, foundProduct){
            let arr=foundProduct.map((val)=> val.product_category);
            let aa=arr.filter((val,i,arr)=>  arr.indexOf(val)==i );
            console.log(aa);
            res.render('admin/addblend.ejs', {
                product : aa,
                moment : moment
              });
            })
          });
          // get ajex
app.get('/category/inventory/products/:cat', (req, res)=> {
          Product.find({product_category:req.params.cat}, function(err, data) {
            res.send({
               data:data
              });
             });
            });
app.post('/add/blend', function(req, res){
              console.log(req.body);

              var blend = new Blend();
                  blend.final_product_name = req.body.final_product_name;
                  blend.netqty = req.body.netqty;
                  // blend.status = "pending";
                  blend.added_date = new Date();
                  let arr=[];
                  let productname=arr.concat(req.body.productname);
                  let product_category=arr.concat(req.body.product_category);
                  let productqty=arr.concat(req.body.productqty);

              blend.save(function(err, doc){
                if(err) res.json(err);
                else {
                  console.log(doc);
                  for(var i=0;i<productname.length;i++){
                  Blend.updateOne({_id:doc.id},{$push:{product_info:{product_category:product_category[i],productname:productname[i],productqty:productqty[i]}}},function(err,data){
                  if(err) res.json(err);
                  else{
                    console.log(data);
                  }
                  })
                }
                  res.redirect('/admin/add/blend'); // redirect to the admin page
                }
              });
              // res.redirect('/admin/add/blend'); // redirect to the admin page
            });
      //Display Blends


app.get('/view/blend', (req, res) => {
      Blend.find({}, function(err, data){
        res.render('admin/viewblends.ejs', {
            data : data,
            moment : moment
          });
        })
      });


      //this is to ctreate Bom display
app.get('/create/bom', (req, res) => {
      Blend.find({}, function(err, data){
        res.render('admin/create_bom.ejs', {
            data :data ,
            moment : moment
          });
        })
      });

      //to get final product quantities
app.get('/finalproducts/quantities/:name', (req, res) => {
      Blend.find({final_product_name:req.params.name}, function(err, data){
        console.log(data);
        res.send({
            data :data
          });
        })
      });

      //this is to ctreate Bom action
app.post('/create/bom', (req, res) => {
        console.log(req.body);
      Blend.findOne({final_product_name:req.body.productname}, function(err, data){
        console.log(data);
      let blend_info=[];
      let order_info=[];
      let total_productqty
      let netqty=order_info.concat(req.body.netqty);
      let sku_code=order_info.concat(req.body.sku_code);
      let number_of_order=order_info.concat(req.body.number_of_order);
      for(var i=0;i<netqty.length;i++){
        console.log("i=",i);
        order_info.push({netqty:netqty[i],sku_code:sku_code[i],number_of_order:number_of_order[i]})
      }
      data.product_info.forEach((prod,index)=>{
        console.log("index=",index);
         total_productqty=0;
        order_info.forEach((order)=>{
          console.log("asnxjnjs");

           total_productqty+=((order.netqty/100) * prod.productqty * order.number_of_order);
        })
        blend_info.push({product_category:prod.product_category,productname:prod.productname,total_productqty:total_productqty})
        console.log(blend_info);
        if(data.product_info.length== index + 1){
          var newRequestbom=new Requestbom ({final_product_name:req.body.productname,netqty:req.body.netqty,number_of_order:req.body.number_of_order,order_info:order_info,blend_info:blend_info,status:"pending",added_date: new Date()});
           newRequestbom.save(newRequestbom,function(err,bomdata){
             console.log("bomdata");
             console.log(bomdata);
             res.redirect("/admin/create/bom");
         })
        }
      })
        })
      });

      //To display pending BOMs
app.get('/pending/bom', (req, res) => {
      Requestbom.find({status:"pending"}, function(err, data){
        if(err) throw err;
        console.log(data);
        res.render('admin/view_created_bom.ejs', {
            data :data,
            moment : moment
          });
        })
      });
      //To display pending BOMs
app.get('/completed/bom', (req, res) => {
      Requestbom.find({status:"done"}, function(err, data){
        if(err) throw err;
        console.log(data);
        res.render('admin/view_created_bom.ejs', {
            data :data,
            moment : moment
          });
        })
      });
      // View created bom blend
app.get('/view/created/bom/blend/:id', (req, res) => {
        console.log(req.params.id);
      Requestbom.findOne({_id:req.params.id}, function(err, data){
        if(err) throw err;
        console.log(data);
        res.render('admin/view_created_bom_blend.ejs', {
            data :data,
            moment : moment
          });
        })
      });

      //AJAX TO GET ALL Category inventory
app.get('/inventory/all/categories', (req, res)=> {
      Product.find({}, function(err, data) {
        res.send({
           data:data
          });
         });
        });

        app.get('/view/order', (req, res)=> {
          //  console.log(req.params);
          Vendor.find({"product_info.productstatus":"approved","product_info.orderstatus":{$ne:"open"}}, function(err, foundMaterial) {
           res.render('admin/vieworders.ejs', {
             data:foundMaterial,
             moment : moment
            });
           });
         });
  
         app.get('/view/created_order', (req, res)=> {
          //  console.log(req.params);
          Order.find({order_status:{$in:["open","intransit","received"]}}, function(err, foundMaterial) {
           res.render('admin/view_created_order.ejs', {
             data:foundMaterial,
             moment : moment
            });
           });
         });

         app.get('/add/lrstatus/:id', (req, res)=> {
          console.log(req.params.id);
         Order.find({_id:req.params.id}, function(err, data) {
         console.log(data);
         res.send({
           data : data,
           moment : moment
       });
       });
     });

     app.post('/added/lrstatus/:id', (req, res)=> {
       console.log(req.params.id);
      Order.findOneAndUpdate({_id:req.params.id},{
        $set : {
                lrno : req.body.lrno,
                order_status : "intransit",
                intransit_date : new Date()
        }},
        {new: true, upsert:true},function(err, statuss){
          if(err){
            console.log(err);
          }
          else{
            res.redirect('/admin/view/created_order');
          }
        }
      )


      app.get('/add/receivedqty/:id', (req, res)=> {
       console.log(req.params.id);
      Order.find({_id:req.params.id}, function(err, data) {
      console.log(data);
      res.send({
        data : data,
        moment : moment
    });
    });
  });

  app.post('/added/receivedqty/:id', (req, res)=> {
    console.log(req.params.id);
   Order.findOneAndUpdate({_id:req.params.id},{
     $set : {
             
             order_status : "received",
             receivedqty : req.body.receivedqty,
             productqty : req.body.productqty,
             received_date : new Date()
     }},
     {new: true, upsert:true},function(err, statuss){
       if(err){
         console.log(err);
       }
       else{
         res.redirect('/admin/view/created_order');
       }
     }
   )
  });

 });

// To approve vendor product
app.get('/approve/vendors/product', (req, res)=> {
      Vendor.updateOne({_id:req.query.a,"product_info._id":req.query.b},{$set:{"product_info.$.productstatus":"approved"}}, function(err, data) {
        res.redirect("/admin/view/vendor");
         });
        });
        // To approve vendor product
        app.get('/reject/vendors/product', (req, res)=> {
              Vendor.updateOne({_id:req.query.a,"product_info._id":req.query.b},{$set:{"product_info.$.productstatus":"rejected"}}, function(err, data) {
                res.redirect("/admin/view/vendor");
                 });
                });
                // To approve packvendor product
                app.get('/approve/packvendors/product', (req, res)=> {
                      Packvendor.updateOne({_id:req.query.a,"product_info._id":req.query.b},{$set:{"product_info.$.productstatus":"approved"}}, function(err, data) {
                        res.redirect("/admin/view/vendor");
                         });
                        });
                        // To approve packvendor product
                        app.get('/reject/packvendors/product', (req, res)=> {
                              Packvendor.updateOne({_id:req.query.a,"product_info._id":req.query.b},{$set:{"product_info.$.productstatus":"rejected"}}, function(err, data) {
                                res.redirect("/admin/view/vendor");
                                 });
                                });
module.exports = app;
